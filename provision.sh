#!/bin/bash

ansible-pull -i 127.0.0.1, -U https://gitlab.com/tcc-version-1.0/ansible-provisioner-nginx.git provision.yml

cat > /etc/cron.d/ansible-pull <<EOF
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

*/5 * * * * root ansible-pull -i 127.0.0.1, -U https://gitlab.com/tcc-version-1.0/ansible-provisioner-nginx.git provision.yml
EOF

