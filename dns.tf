
data "aws_route53_zone" "domain" {
  name = "thecloudcoach.tv"
}

resource "aws_route53_record" "bastion" {
  zone_id = data.aws_route53_zone.domain.id
  name    = "bastion"
  type    = "A"
  ttl     = 300
  records = [data.terraform_remote_state.management.outputs.bastion-host-eip]
}

resource "aws_route53_record" "alb" {
  zone_id = data.aws_route53_zone.domain.id
  name    = "thecloudcoach.tv"
  type    = "A"

  alias {
    name                   = aws_lb.webservers.dns_name
    zone_id                = aws_lb.webservers.zone_id
    evaluate_target_health = true
  }
}

