
resource "aws_lb" "webservers" {
  name               = "tcc-stream-v1"
  internal           = false
  load_balancer_type = "application"

  security_groups = [
    data.terraform_remote_state.management.outputs.sg-https
  ]

  subnets = [
    data.terraform_remote_state.management.outputs.subnet-az-a-public,
    data.terraform_remote_state.management.outputs.subnet-az-b-public
  ]
}

resource "aws_lb_target_group" "webservers" {
  name     = "tcc-stream-v1"
  port     = 80
  protocol = "HTTP"
  vpc_id   = data.terraform_remote_state.management.outputs.vpc
}

resource "aws_lb_listener" "https" {
  load_balancer_arn = aws_lb.webservers.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = aws_acm_certificate.alb.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.webservers.arn
  }
}
