
provider "aws" {
  region = "ap-southeast-2"
}

terraform {
  backend "s3" {
    bucket         = "tcc-stream-v10-state"
    key            = "workload/terraform.tfstate"
    region         = "ap-southeast-2"
    dynamodb_table = "tcc-stream-v10-state-locking"
  }
}
