
data "terraform_remote_state" "management" {
  backend = "s3"
  config = {
    bucket = "tcc-stream-v10-state"
    key    = "management/terraform.tfstate"
    region = "ap-southeast-2"
  }
}

