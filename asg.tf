
data "local_file" "userdata" {
  filename = "${path.module}/provision.sh"
}

resource "aws_launch_template" "static-website" {
  name          = "static-website"
  image_id      = "ami-0cdfb861146115b31" # our base image
  instance_type = "t2.small"
  user_data     = data.local_file.userdata.content_base64

  vpc_security_group_ids = [
    data.terraform_remote_state.management.outputs.sg-all-outbound,
    data.terraform_remote_state.management.outputs.sg-http,
    data.terraform_remote_state.management.outputs.sg-ssh-internal
  ]
}

resource "aws_autoscaling_group" "static-website" {
  desired_capacity = 2
  max_size         = 2
  min_size         = 1
  vpc_zone_identifier = [
    data.terraform_remote_state.management.outputs.subnet-az-a-private,
    data.terraform_remote_state.management.outputs.subnet-az-b-private,
  ]

  launch_template {
    id      = aws_launch_template.static-website.id
    version = "$Latest"
  }

  target_group_arns = [
    aws_lb_target_group.webservers.arn,
  ]

  tag {
    key                 = "Name"
    value               = "static-website-asg-instance"
    propagate_at_launch = true
  }
}

