
resource "aws_acm_certificate" "alb" {
  domain_name       = "thecloudcoach.tv"
  validation_method = "DNS"
}

resource "aws_route53_record" "acm-validation" {
  name    = aws_acm_certificate.alb.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.alb.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.domain.id
  records = [aws_acm_certificate.alb.domain_validation_options.0.resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "alb" {
  certificate_arn         = aws_acm_certificate.alb.arn
  validation_record_fqdns = [aws_route53_record.acm-validation.fqdn]
}
